from django.contrib import admin


from meal_plans.models import MealPlan


class MealPlanAdmin(admin.ModelAdmin):
    pass


# Register your models here.

admin.site.register(MealPlan)
